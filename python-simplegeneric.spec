%if 0%{?fedora} > 15 || 0%{?rhel} >= 7
%global with_python3 1
%endif

%global modname simplegeneric

Name:           python-simplegeneric
Version:        0.8
Release:        7.2%{?dist}
Summary:        Simple generic functions (similar to Python's own len(), pickle.dump(), etc.)

Group:          Development/Languages
License:        Python or ZPLv2.1
URL:            http://cheeseshop.python.org/pypi/simplegeneric
Source0:        http://pypi.python.org/packages/source/s/%{modname}/%{modname}-%{version}.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  /usr/bin/2to3

%description
The simplegeneric module lets you define simple single-dispatch generic
functions, akin to Python's built-in generic functions like len(), iter() and
so on. However, instead of using specially-named methods, these generic
functions use simple lookup tables, akin to those used by e.g. pickle.dump()
and other generic functions found in the Python standard library.


%package -n python%{python3_pkgversion}-%{modname}
Summary:        Simple generic functions (similar to Python's own len(), pickle.dump(), etc.)

Group:          Development/Languages
License:        Python or ZPLv2.1

%description -n python%{python3_pkgversion}-%{modname}
The simplegeneric module lets you define simple single-dispatch generic
functions, akin to Python's built-in generic functions like len(), iter() and
so on. However, instead of using specially-named methods, these generic
functions use simple lookup tables, akin to those used by e.g. pickle.dump()
and other generic functions found in the Python standard library.


%prep
%setup -q -n %{modname}-%{version}

2to3 --write --nobackups .
sed -i "s/file(/open(/g" setup.py
find . -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'

%build
%{__python3} setup.py build


%install
rm -rf %{buildroot}
%{__python3} setup.py install --skip-build --root %{buildroot}

%check
PYTHONPATH=$(pwd) %{__python3} setup.py test

%clean
rm -rf %{buildroot}


%files -n python%{python3_pkgversion}-%{modname}
%defattr(-,root,root,-)
%doc README.txt
%{python3_sitelib}/__pycache__/simplegeneric.cpython*
%{python3_sitelib}/simplegeneric.py*
%{python3_sitelib}/simplegeneric-%{version}-py?.?.egg-info


%changelog
* Thu Oct 24 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.8-7.2
- Drop support for python3.4, python2

* Fri Nov 30 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 0.8-7.1
- Update to support python 3.6

* Thu Jan 09 2014 Dennis Gilmore <dennis@ausil.us> - 0.8-7
- no python3 in RHEL 7

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sat Aug 04 2012 David Malcolm <dmalcolm@redhat.com> - 0.8-5
- rebuild for https://fedoraproject.org/wiki/Features/Python_3.3

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 27 2012 Thomas Spura <tomspur@fedoraproject.org> - 0.8-3
- be more explicit in files section
- add python3 subpackage (#785056)

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Dec 14 2011 Luke Macken <lmacken@redhat.com> - 0.8-1
- Update to 0.8 (#735066)

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Sep 30 2010 Luke Macken <lmacken@redhat.com> - 0.7-1
- Update to 0.7
- Run the unit tests

* Thu Jul 22 2010 Thomas Spura <tomspur@fedoraproject.org> - 0.6-3
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild
- missing BR: python-devel

* Mon Apr 19 2010 Luke Macken <lmacken@redhat.com> - 0.6-2
- Change license from 'PSF or ZPL' to 'Python or ZPLv2.1'

* Tue Apr 13 2010 Luke Macken <lmacken@redhat.com> - 0.6-1
- Initial package
